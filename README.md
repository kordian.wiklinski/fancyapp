# Kordian Wikliński

Ionic 2 app, using Firebase as primary DB

Funcions
  - login as test user
  - possibility to log in if user has created account on Firebase console
  - Write messages on wall
  - Read old message
  - Delete all message

TODO list:
  - Createing new accounts, Auth with Google and Facebook
  - move authenticatin to serice
  - create users list
  - etc.

License
----

MIT


**Free Software, Hell Yeah!**

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

