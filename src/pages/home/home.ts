import { Component } from '@angular/core';

import { NavController, AlertController } from 'ionic-angular';
import { AngularFire, AuthProviders, AuthMethods, FirebaseListObservable } from 'angularfire2';

 declare var moment: any;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  loginName : string = 'testuser@test.pl';
  password : string = 'testuser@test.pl';
  authSub : any;
  activeUser : any = null;

  messagesSub : any;

  messages : any = [];

  newMessage : string;

  signUpEmail : string;
  signUpPassword : string;
  loginAction : boolean = true;

  constructor(
      public navCtrl: NavController,
      private af : AngularFire,
      public alertCtrl: AlertController) {
    this.authSub = this.af.auth.subscribe(auth => {
      this.activeUser = JSON.parse(JSON.stringify(auth));

      if(this.activeUser) {
        //console.log(this.activeUser);

        this.getMessages();

      }
    });

  }

  login() : void {

    this.af.auth.login({ email: this.loginName, password: this.password })
        .catch((error) => {
          alert('Error while login');
        });

  }

  signUp(email: string, password: string) {
    var creds: any = { email: this.signUpEmail, password: this.signUpPassword };
    this.af.auth.createUser(creds);
  }

  loginGoogle() : void {
    // this.af.auth.login({ email: this.loginName, password: this.password })
    //     .catch((error) => {
    //       alert('Error while login');
    //     });

    // this.af.auth.login({
    //   provider: AuthProviders.Google,
    //   method: AuthMethods.Redirect
    // })
    //     .catch((error) => {
    //       alert('Error while login');
    //     });;
  }

  logout() : void {
    this.af.auth.logout();
    this.activeUser = null;
    this.messagesSub.unsubscribe();
    this.messages = null;
  }

  loginActionToogle(loginaction : boolean) : void {
    this.loginAction = loginaction;
  }

  getMessages() : void {
    let messagesSub = this.af.database.list('/messages');

    this.messagesSub = messagesSub.subscribe(response => {
      console.log(response);
      this.messages = response;
    })

  }

  addMessage() : void {

    let timestamp = new Date().getTime();

    let newMessage = {
        user : this.activeUser.auth.email,
        text : this.newMessage,
        date : timestamp
    }
    //console.log(newMessage);

    this.af.database.object('/messages/'+timestamp).set(newMessage);

    this.newMessage = '';
  }

  showAlert() {
    let alert = this.alertCtrl.create({
      title: 'Permission denied',
      subTitle: 'Only admin is allowed to remove messages',
      buttons: ['OK']
    });
    alert.present();
  }


  removeMessage() : void {
    if(this.activeUser.auth.email !== 'wikordian@gmail.com') {
      this.showAlert();
      return;
    }
    //this.af.database.object('/messages').remove();
  }

  formatDate(value : number) : any  {
    let created = new Date(value);
    let validDate = moment(created).format("DD/MM/YYYY HH:MM:ss");

    // TODO replace invalid Data
    //(validDate === 'Invalid date') && ( validDate = moment(945651609000).format("DD/MM/YYYY HH:MM:ss"));
    return validDate;
  }

}
